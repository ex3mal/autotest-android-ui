import pytest
import os
import appium
from appium import webdriver


desired_caps = dict(
    platformName='Android',
    platformVersion='11',
    automationName='uiautomator2',
    deviceName='sdk_gphone_x86', #Nexus 5 API 30
    app='D:/05.ru/622c67d3-ea05-41ec-b8e6-9cf1de02681b.apk',
    noReset='false',
    newCommandTimeout = "30000"
)

@pytest.fixture(scope="function", autouse=True)
def get_driver(request):
    driver: webdriver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
    request.cls.driver = driver
    

    #driver.close()
