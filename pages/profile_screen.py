import allure
import pytest

from .base import TestBase


class TestProfile(TestBase):

    @allure.step('Открытие окна авторизации')
    def test_go_to_sign_in(self):
        self.test_await_action_with_element_by_id('profile_unauth_sign_in_button', 0, 'click')
        self.test_screenshot()


    @allure.step('Переход в Сравнение')
    def test_go_to_comparison(self):
        self.test_await_action_with_element_by_id('profile_menu_comparison_item', 0, 'click')
        self.test_screenshot()
    
    # @allure.step('Переход в категорию в сравнении')
    # def test_go_to_comparison_category(self):
    #     self.test_await_action_with_element_by_id('comparison_category_name_label', 0, 'click')
    #     self.test_screenshot()


    @allure.step('Переход из пустого сравнения в каталог')
    def test_comparison_go_to_catalog(self):
        self.test_await_action_with_element_by_id('empty_view_action_button', 0, 'click')
        self.test_screenshot()


    # @allure.step('Переход в Корзину')
    # def test_go_to_cart(self):
    #     self.test_await_action_with_element_by_id('profile_menu_cart_item', 0, 'click')


    @allure.step('Переход в Выбор города')
    def test_go_to_change_city(self):
        self.test_await_action_with_element_by_id('profile_menu_city_item', 0, 'click')
        self.test_screenshot()


    
    @allure.step('Переход в Заказы')
    def test_go_to_orders(self):
        self.test_click_by_coordinates(200, 550)
        self.test_screenshot()

    @allure.step('Выбор города')
    def test_change_city(self, city):
        try:
            self.test_swipe_to_list_element('city_name_label', city).click()
            self.test_screenshot()
        except:
            self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[4]', \
                0, 'click', method='find_elements_by_xpath')


    @allure.step('Переход в Помощь')
    def test_go_to_help(self):
        #self.test_swipe_action_with_element_by_id('profile_menu_help_item', 0, 'click')
        self.test_await_action_with_element_by_id('profile_menu_help_item', 0, 'click')
        self.test_screenshot()
    

    @allure.step('Переход в Магазины и пункты выдачи')
    def test_go_to_help_stories(self):
        self.test_await_action_with_element_by_id('help_menu_stores_item', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Переход в Доставку')
    def test_go_to_help_delivery(self):
        self.test_await_action_with_element_by_id('help_menu_delivery_item', 0, 'click')
        self.test_screenshot()

    @allure.step('Переход в Бонусы')
    def test_go_to_help_bonuses(self):
        self.test_await_action_with_element_by_id('help_menu_bonuses_item', 0, 'click')
        self.test_screenshot()

    @allure.step('Переход в Возврат товара')
    def test_go_to_help_product_return(self):
        self.test_await_action_with_element_by_id('help_menu_product_return_item', 0, 'click')
        self.test_screenshot()

    @allure.step('Переход в Кредит')
    def test_go_to_help_credit(self):
        self.test_await_action_with_element_by_id('help_menu_credit_item', 0, 'click')
        self.test_screenshot()

    @allure.step('Переход в Рассрочку')
    def test_go_to_help_installment(self):
        self.test_await_action_with_element_by_id('help_menu_installment_item', 0, 'click')
        self.test_screenshot()

    @allure.step('Переход в Службу поддержки')
    def test_go_to_help_support_call(self):
        self.test_await_action_with_element_by_id('help_menu_support_call_item', 0, 'click')
        self.test_screenshot()

    @allure.step('Назад')
    def test_help_toolbar_back(self):
        self.test_await_action_with_element_by_id('main_toolbar_back_icon', 0, 'click')
        self.test_screenshot()

    @allure.step('Выбор города через поисковую строку')
    def test_change_city_with_search(self, city=''):
        self.test_await_action_with_element_by_id('select_city_search_input', 0, 'send_keys', city)
        self.test_await_action_with_element_by_id('city_name_label', 0, 'click')
        self.test_screenshot()
    



class TestSignIn(TestBase):

    @allure.step('Ввод номера телефона')
    def test_sign_in_SMS_phone_input(self, phone='9898875410'):
        self.test_await_action_with_element_by_id('sign_in_by_sms_phone_input', 0, 'send_keys', phone)
        self.test_screenshot()

    
    @allure.step('Получить код')
    def test_sign_in_SMS_get_code(self):
        self.test_await_action_with_element_by_id('sign_in_by_sms_get_code_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Повторная отправка кода')
    def test_sign_in_SMS_repeat_send(self):
        self.test_timeout(100)
        self.test_await_action_with_element_by_id('enter_code_resend_sms_label', 0, 'click')
        self.test_screenshot()

    @allure.step('Ввод кода подтверждения')
    def test_sign_in_SMS_code_input(self, code='1111'):
        self.test_await_action_with_element_by_id('enter_code_input', 0, 'send_keys', code)
        self.test_screenshot()


    @allure.step('Войти по паролю')
    def test_sign_in_go_to_password_container(self):
        self.test_await_action_with_element_by_id('sign_in_by_sms_enter_by_password_container', 0, 'click')


    @allure.step('Ввод логина')
    def test_sign_in_login_input(self, login):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', login)

    @allure.step('Ввод пароля')
    def test_sign_in_password_input(self, password):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', password)

    @allure.step('Авторизация')
    def test_sign_in_action_button(self):
        self.test_await_action_with_element_by_id('sign_in_by_password_enter_button', 0, 'click')


    @allure.step('Забыли пароль')
    def test_go_to_recovery_password(self):
        self.test_await_action_with_element_by_id('main_input_right_title_label', 0, 'click')


    @allure.step('Восстановление по номеру')
    def test_choice_recovery_by_phone(self):
        self.test_await_action_with_element_by_id('enter_login_phone_button', 0, 'click')


    @allure.step('Восстановление по почте')
    def test_choice_recovery_by_email(self):
        self.test_await_action_with_element_by_id('enter_login_email_button', 0, 'click')


    @allure.step('Ввод номера телефона или почты для восстановления пароля')
    def test_recovery_phone_email_input(self, login):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', login)


    @allure.step('Переход на следующий этап восстановления пароля')
    def test_recovery_phone_email_next_button(self):
        self.test_await_action_with_element_by_id('enter_login_next_button', 0, 'click')


    @allure.step('Ввод кода подтверждения для восстановления пароля')
    def test_recovery_code_input(self, code='111111'):
        self.test_await_action_with_element_by_id('enter_code_input', 0, 'send_keys', code)
    

    @allure.step('Переход на следующий этап восстановления пароля')
    def test_recovery_code_next_button(self):
        self.test_await_action_with_element_by_id('enter_code_next_button', 0, 'click')


    @allure.step('Ввод нового пароля')
    def test_recovery_new_password_input(self, password):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', password)


    @allure.step('Повтор нового пароля')
    def test_recovery_repeat_new_password_input(self, password):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', password)


    @allure.step('Обновить пароль')
    def test_recovery_action_button(self):
        self.test_await_action_with_element_by_id('recovery_action_button', 0, 'click')
        if self.test_check_page_with_element_by_id('snackbar_text'):
            pytest.skip('Пароль должен быть не менее 8 символов длиной...')


class TestSignUp(TestBase):

    @allure.step('Переход к регистрации')
    def test_sign_up(self):
        self.test_await_action_with_element_by_id('sign_in_sign_up_label', 0, 'click')


    @allure.step('Ввод номера')
    def test_sign_up_login_phone_input(self, phone):
        self.test_await_action_with_element_by_id('enter_login_phone_input', 0, 'send_keys', phone)


    @allure.step('Переход на следующий этап')
    def test_sign_up_login_next_button(self):
        self.test_await_action_with_element_by_id('enter_login_next_button', 0, 'click')
        if self.test_check_page_with_element_by_id('snackbar_text'):
            pytest.skip('Пользователь уже зарегистирован')


    @allure.step('Ввод кода подтверждения')
    def test_sign_up_code_input(self, code):
        self.test_await_action_with_element_by_id('enter_code_input', 0, 'send_keys', code)


    @allure.step('Переход на следующий этап')
    def test_sign_up_code_next_button(self):
        self.test_await_action_with_element_by_id('enter_code_next_button', 0, 'click')


    @allure.step('Ввод имени')
    def test_sign_up_name_input(self, name):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', name)


    @allure.step('Ввод Email')
    def test_sign_up_email_input(self, email):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', email)


    @allure.step('Ввод пароля')
    def test_sign_up_password_input(self, password):
        self.test_await_action_with_element_by_id('main_text_input', 2, 'send_keys', password)

    
    @allure.step('Регистрация')
    def test_sign_up_action_button(self):
        self.test_await_action_with_element_by_id('sign_up_action_button', 0, 'click')
