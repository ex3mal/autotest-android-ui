import allure

from .base import TestBase


class TestMain(TestBase):
        
    @allure.step('Выбор города')
    def test_choice_city(self):
        self.test_await_action_with_element_by_id('select_city_skip_label', 0, 'click')
        self.test_screenshot()

    @allure.step('Выбор города')
    def test_choice_other_city(self, name_town):
        #self.test_await_action_with_element_by_id('city_name_label', number_town, 'click')
        self.test_swipe_to_list_element('city_name_label', name_town).click()
        self.test_screenshot()


    @allure.step('Поиск')
    def test_search(self):
        self.test_await_action_with_element_by_id('home_search_container', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Поисковый запрос')
    def test_search_input(self, search_query):
        self.test_await_action_with_element_by_id('search_toolbar_query_input', 0, 'send_keys', search_query)
        self.test_screenshot()

    
    @allure.step('Переход в Даркстор')
    def test_go_to_darkstore(self):
        self.test_await_action_with_element_by_id('home_go_to_darkstore_image', 0, 'click')
        self.test_screenshot()

    
    @allure.step('Переход в категорию') #Использовать
    def test_go_to_category(self, name_category):
        try:
            self.test_swipe_to_list_element('category_name_label', name_category).click()
            self.test_screenshot()
        except:
            self.test_swipe_horizont()
            self.test_swipe_to_list_element('category_name_label', name_category).click()
            self.test_screenshot()


    @allure.step("Акции")
    def test_go_to_promotions(self):
        self.test_await_action_with_element_by_id('home_banner_image', 0, 'click')
        self.test_screenshot()

    @allure.step("Проверка наличии акции")
    def test_check_promotions(self):
        self.test_check_text_page_with_element_by_id('discount_details_image', 0)
        self.test_screenshot()

    
    @allure.step('Переход в баннер "Телефоны и гаджеты"')
    def test_go_to_phone_banner(self):
        #self.test_await_action_with_element_by_id('home_category_name_label', 0, 'click')
        self.test_swipe_action_with_element_by_id('home_category_content_container',  0, 'click')
        #self.test_await_action_with_element_by_id('home_category_content_container', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Открытие карточки товара в блоке "Популярные товары"')
    def test_go_to_product_popular(self):
        #self.test_await_action_with_element_by_id('home_category_name_label', 0, 'click')
        #self.test_swipe_to_list_element('', '').click()
        self.test_swipe_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[1]/android.view.ViewGroup', \
            0, 'click', method='find_elements_by_xpath')
        self.test_screenshot()


    @allure.step("Переход в Систему бонусов")
    def test_go_to_bonus(self):
        #self.test_swipe_action_with_element_by_id('bonus_advantages_cart_image', 0, 'click')
        self.test_swipe_to_list_element('bonus_advantages_cart_title_label', '100%').click()
        self.test_screenshot()

    @allure.step("Проверка перехода в систему бонусов")
    def test_check_go_to_bonus(self):
        self.test_check_text_page_with_element_by_id('bonus_card_toolbar_title_label', 0, 'Бонусная карта')
        self.test_screenshot()