import allure

from .base import TestBase


class TestCartIdle(TestBase):

    @allure.step('Проверка пустой корзины')
    def test_check_idle_cart(self):
        self.test_check_text_page_with_element_by_id('empty_view_title_label', 0, 'В корзине нет товаров')
        self.test_screenshot()


    @allure.step('Переход в каталог из корзины')
    def test_go_to_catalog(self):
        self.test_await_action_with_element_by_id('empty_view_action_button', 0, 'click') 
        self.test_screenshot()



class TestCart(TestBase):

    @allure.step('Проверка наличия товара в корзине')
    def test_check_cart(self):
        self.test_check_text_page_with_element_by_id('cart_product_delete_label', 0, 'Удалить')
        self.test_screenshot()


    @allure.step('Переход в карточку товара из корзины')
    def test_go_to_product(self):
        self.test_await_action_with_element_by_id('cart_product_image', 0, 'click') 
        self.test_screenshot()


    @allure.step('Добавление в сравнение из корзины') # не работает
    def test_add_to_comparison(self):
        self.test_await_action_with_element_by_id('checkable_icon_image', 0, 'click')
        self.test_screenshot()

    @allure.step('Добавление в избранное из корзины') # не работает
    def test_add_to_favorites(self):
        self.test_await_action_with_element_by_id('checkable_icon_image', 1, 'click') 
        self.test_screenshot()


    @allure.step('Уменьшение количества товара в корзине')
    def test_counter_minus(self):
        self.test_await_action_with_element_by_id('cart_counter_minus_image', 0, 'click')
        self.test_screenshot() 

    
    @allure.step('Увеличение количества товара в корзине')
    def test_counter_plus(self):
        self.test_await_action_with_element_by_id('cart_counter_plus_image', 0, 'click') 
        self.test_screenshot()

    
    @allure.step('Удаление товара из корзины')
    def test_product_delete(self):
        self.test_await_action_with_element_by_id('cart_product_delete_label', 0, 'click') 
        self.test_screenshot()

    @allure.step('Очистка всей корзины корзины')
    def test_product_clear(self):
        self.test_await_action_with_element_by_id(self.LOCATOR_CART_CLEAR, 0, 'click') 
        self.test_screenshot()


    @allure.step('Переход к оформлению заказа')
    def test_go_to_checkout(self):
        #self.test_await_action_with_element_by_id('cart_total_price_action_button', 0, 'click')
        #self.test_swipe_action_with_element_by_id('cart_total_price_action_button', 0, 'click')  
        self.test_swipe_action_with_element_by_id('cart_total_price_action_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Ввод промокода')
    def test_sign_in_promo_input(self, promo='sotrudnik15'):
        self.test_await_action_with_element_by_id('additional_promo_code_input', 0, 'send_keys', promo)
        self.test_screenshot()



class TestCartCheckout(TestBase):

    @allure.step('Ввод города') # выбран автоматически
    def test_delivery_town(self, town):
        self.test_await_action_with_element_by_id('order_delivery_city_clear_image', 0, 'click')
        self.test_timeout(2)
        self.test_await_action_with_element_by_id('order_delivery_city_input', 0, 'send_keys', town)
        self.test_timeout(2)
        self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView', \
            0, 'click', method='find_elements_by_xpath')
        self.test_screenshot()
    
    @allure.step('Выбор адреса')
    def test_delivery_address(self):
        self.test_click_by_coordinates(400, 1500)
        self.test_screenshot()

    
    @allure.step('Выбор способа доставки') # Не использовать (одинаковые локаторы)
    def test_delivery_method(self, delivery_method):
        if delivery_method == 'Курьерская доставка 05.ru':
            #self.test_await_action_with_element_by_id('delivery_type_radiobutton', 0, 'click')
            #self.test_swipe_to_list_element('selection_single_name_label', delivery_method).click()
            self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.RadioButton', \
                0, 'click', method='find_element_by_xpath')
        if delivery_method == 'Самовывоз':
            #self.test_await_action_with_element_by_id('delivery_type_radiobutton', 1, 'click')
            #self.test_swipe_to_list_element('selection_single_name_label', delivery_method).click()
            
            self.test_await_action_with_element_by_id('selection_single_radiobutton', \
                1, 'click')
            
        self.test_screenshot()
    

    @allure.step('Выбор магазина для самовывоза')
    def test_delivery_pickup_choice(self):
        self.test_await_action_with_element_by_id('address_arrow_image', 0, 'click')
        self.test_screenshot()


    @allure.step('Переход на следующий этап')
    def test_delivery_next_button(self):
        self.test_await_action_with_element_by_id('order_delivery_next_button', 0, 'click')
        self.test_screenshot()


    @allure.step('Ввод адреса доставки')
    def test_address_input(self, address):
        self.test_await_action_with_element_by_id('address_search_input', 0, 'send_keys', address)
        self.test_timeout(2)
        self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout[2]/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[1]', \
            0, 'click', method='find_elements_by_xpath')
        self.test_screenshot()
    
    @allure.step('Выбор адреса доставки')
    def test_address_choice(self):
        self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout[3]/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout/android.widget.FrameLayout[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]', \
            0, 'click', method='find_elements_by_xpath')
        self.test_screenshot()


    @allure.step('Переход на следующий этап')
    def test_address_next_button(self):
        self.test_await_action_with_element_by_id('order_map_next_button', 0, 'click')
        self.test_screenshot()

    
    @allure.step('Ввод ФИО') # Не использовать (одинаковые локаторы)
    def test_names_input(self, fio):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', fio)

    @allure.step('Ввод Email') # Не использовать (одинаковые локаторы)
    def test_email_input(self, email):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', email)

    @allure.step('Ввод телефонного номера') # Не использовать (одинаковые локаторы)
    def test_phone_input(self, phone):
        self.test_await_action_with_element_by_id('main_text_input', 2, 'send_keys', phone)


    @allure.step('Переход на следующий этап')
    def test_contacts_next_button(self):
        self.test_await_action_with_element_by_id('order_contacts_action_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Выбор способа оплаты')
    def test_payment_method(self):
        self.test_await_action_with_element_by_id('selection_single_radiobutton', 1, 'click')
        self.test_screenshot()

    @allure.step('Переход на следующий этап')
    def test_payment_next_button(self):
        self.test_await_action_with_element_by_id('order_payment_next_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Оформление заказа')
    def test_checkout(self):
        self.test_await_action_with_element_by_id('order_total_create_button', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Оформление заказа')
    def test_checkout(self):
        self.test_await_action_with_element_by_id('order_total_create_button', 0, 'click')
        self.test_screenshot()

    
    @allure.step('Ввод фио на шаге "Контакты" ')
    def test_input_fio_in_contacts_step(self, name='Тест'):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', name)
        self.test_screenshot()
    
    @allure.step('Ввод номера телефона на шаге "Контакты" ')
    def test_input_phone_number_in_contacts_step(self, name='9898875410'):
        self.test_await_action_with_element_by_id('main_text_input', 2, 'send_keys', name)
        self.test_screenshot()