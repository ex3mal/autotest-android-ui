import allure
import pytest

from .base import TestBase


class TestAuthorizedProfile(TestBase):

    @allure.step('Тулбар уведомлений')
    def test_go_to_toolbar_notifications(self):
        self.test_await_action_with_element_by_id('main_toolbar_menu_icon', 0, 'click')
        self.test_screenshot()


    @allure.step('Переход в Историю заказов')
    def test_go_to_order_history(self):
        self.test_swipe_to_list_element('profile_menu_card_title_label', 'Заказы').click()
        self.test_screenshot()

    @allure.step('Переход к заказу')
    def test_go_to_last_order(self):
        #self.test_await_action_with_element_by_id('order_arrow_right_icon', 0, 'click')
        self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/androidx.cardview.widget.CardView[1]/android.view.ViewGroup/android.widget.ImageView',\
            0, 'click', method='find_elements_by_xpath')
        self.test_screenshot()

    @allure.step('Переход на следующий этап')
    def test_order_cancel_next_button(self):
        #self.test_swipe_action_with_element_by_id('order_detail_cancel_button', 0, 'click')
        #self.test_await_action_with_element_by_id('order_detail_cancel_button', 0, 'click')
        self.test_swipe_to_list_element('order_detail_cancel_button', 'Отменить заказ').click()
        self.test_screenshot()

    @allure.step('Выбор причины отмены заказа')
    def test_order_cancel_reason(self, reason_cancel='Другое'):
        self.test_swipe_to_list_element('order_detail_cancel_button', reason_cancel).click()
        self.test_screenshot()

    @allure.step('Описание проблемы')
    def test_order_cancel_problems_input(self, text):
        self.test_swipe_action_with_element_by_id('main_text_input', 0, 'send_keys', text)
        self.test_screenshot()

    @allure.step('Отмена заказа')
    def test_order_cancel_action(self):
        self.test_await_action_with_element_by_id('cancel_order_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Переход на главную')
    def test_order_go_to_main(self):
        self.test_await_action_with_element_by_id('success_action_button', 0, 'click')
        self.test_screenshot()


    @allure.step('Переход в Избранное')
    def test_go_to_favorites(self):
        self.test_swipe_to_list_element('profile_menu_card_title_label', 'Избранное').click()
        self.test_screenshot()
    
    @allure.step('Переход из пустого избранного в каталог')
    def test_favorites_go_to_catalog(self):
        self.test_await_action_with_element_by_id('empty_view_action_button', 0, 'click')

    
    @allure.step('Переход в Бонусы')
    def test_go_to_bonuses(self):
        self.test_swipe_to_list_element('profile_menu_card_title_label', 'Бонусы').click()
        self.test_screenshot()
    

    @allure.step('Переход в Уведомления')
    def test_go_to_notifications(self):
        self.test_await_action_with_element_by_id('profile_menu_notifications_item', 0, 'click')
        self.test_screenshot()

    @allure.step('Checkbox Push-уведомления')
    def test_notifications_push_action(self):
        #self.test_await_action_with_element_by_id('notification_settings_push_checkbox', 0, 'click')
        self.test_swipe_to_list_element('notification_settings_push_checkbox', 'Push-уведомления').click()
        self.test_screenshot()


    @allure.step('Переход к смене пароля')
    def test_go_to_change_password(self):
        self.test_await_action_with_element_by_id('profile_menu_change_password_item', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Ввод старого пароля')
    def test_change_password_old_input(self, password):
        self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.EditText',\
            2, 'send_keys', password, 'find_elements_by_xpath')
        self.test_screenshot()

    @allure.step('Ввод нового пароля')
    def test_change_password_new_input(self, password):
        self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.FrameLayout[2]/android.view.ViewGroup/android.widget.EditText',\
            2, 'send_keys', password, 'find_elements_by_xpath')
        self.test_screenshot()

    @allure.step('Обновить пароль')
    def test_change_password_action_button(self):
        self.test_await_action_with_element_by_id('change_password_action_button', 0, 'click')
        self.test_screenshot()
    

    @allure.step('Переход в настройки')
    def test_go_to_settings(self):
        self.test_await_action_with_element_by_id('profile_auth_settings_image', 0, 'click')
        self.test_screenshot()

    @allure.step('Личная информация') # Не использовать (одинаковые локаторы)
    def test_go_to_personal_info(self):
        self.test_swipe_action_with_element_by_id('profile_menu_personal_info', 0, 'click')

    @allure.step('Ввод фамилии') # Не использовать (одинаковые локаторы)
    def test_personal_surname_input(self, surname):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', surname)

    @allure.step('Ввод имени') # Не использовать (одинаковые локаторы)
    def test_personal_name_input(self, name):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', name)

    @allure.step('Ввод отчества') # Не использовать (одинаковые локаторы)
    def test_personal_patronymic_input(self, patronymic):
        self.test_await_action_with_element_by_id('main_text_input', 2, 'send_keys', patronymic)

    @allure.step('Ввод email') # Не использовать (одинаковые локаторы)
    def test_personal_email_input(self, email):
        self.test_await_action_with_element_by_id('main_text_input', 3, 'send_keys', email)

    @allure.step('Сохранение личной информации') # Не использовать (одинаковые локаторы)
    def test_personal_info_save(self):
        self.test_await_action_with_element_by_id('profile_info_save_button', 0, 'click')



    @allure.step('Выход из профиля')
    def test_exit(self):
        self.test_swipe_action_with_element_by_id('profile_menu_exit_item', 0, 'click')
        #self.test_timeout(2)
        self.test_await_action_with_element_by_id('android:id/button1', 0, 'click')
        self.test_screenshot()

    @allure.step('Проверка выхода из профиля')
    def test_check_exit(self):
        self.test_check_page_with_element_by_id('profile_unauth_sign_in_button')
        self.test_screenshot()
        