import allure

from .base import TestBase


class TestCatalog(TestBase):

    @allure.step('Поиск')
    def test_go_to_search(self):
        self.test_await_action_with_element_by_id('main_toolbar_menu_icon', 0, 'click')
        self.test_screenshot()

    @allure.step('Поисковый запрос')
    def test_search_input(self, search_query):
        self.test_await_action_with_element_by_id('search_toolbar_query_input', 0, 'send_keys', search_query)
        self.test_screenshot()

    
    @allure.step('Переход в категорию')
    def test_category_2(self, name_category):
        #self.test_timeout(1)
        try:
            for num_category in range(0, 8):
                if self.driver.find_elements_by_id('category_name_label')[num_category].text == name_category:
                    self.test_await_action_with_element_by_id('category_name_label', num_category, 'click')
                    break
        except:
            print(self.driver.find_elements_by_id('category_name_label')[0].text)


    @allure.step('Переход в категорию') #Использовать
    def test_go_to_category(self, name_category):
        self.test_swipe_to_list_element('category_name_label', name_category).click()
        self.test_screenshot()
    
    # @allure.step('Переход в категорию')
    # def test_go_to_category2(self, name):
    #     self.test_swipe_action_with_element_by_id('category_name_label', name)


    @allure.step('Добавление в корзину')
    def test_add_to_cart(self):
        self.test_await_action_with_element_by_id('cart_counter_default_container', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Добавление в корзину второго товара')
    def test_add_to_cart_2(self):
         self.test_swipe()
         self.test_swipe()
         self.test_await_action_with_element_by_id('cart_counter_default_container', 0, 'click')
         self.test_screenshot()
    

    @allure.step('Переход в карточку товара раздела "Телефоны и гаджеты"')
    def test_go_to_product_card(self):
        try:
            self.test_await_action_with_element_by_id('item_catalog_product_name_label', 0, 'click')
            self.test_screenshot()
        except:
            self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView[1]',\
                0, 'click', method='find_elements_by_xpath')
            self.test_screenshot()
    
    @allure.step('Переход в карточку товара раздела "Бизнес и финансы"')
    def test_go_to_product_card_by_text(self, product_name):
        self.test_swipe_to_list_element('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView[1]', \
            product_name)
        self.test_screenshot()

    @allure.step('Увеличение количества товара в каталоге')
    def test_counter_plus(self):
        self.test_await_action_with_element_by_id('cart_counter_plus_button', 0, 'click')
        self.test_timeout(3) 
        self.test_screenshot()   
    
        
    @allure.step('Уменьшение количества товара в каталоге')
    def test_counter_minus(self):
        self.test_await_action_with_element_by_id('cart_counter_minus_button', 0, 'click') 
        self.test_screenshot()   
        
        
      

    
