import allure

from .base import TestBase
from appium.webdriver.common.touch_action import TouchAction



class TestBottomBar(TestBase):

    @allure.step('Переход на главную')
    def test_go_to_bottom_home(self):
        self.test_await_action_with_element_by_id('menu_main_bottom_nav_home', 0, 'click')
        self.test_screenshot()


    @allure.step('Переход в каталог')
    def test_go_to_bottom_catalog(self):
        self.test_await_action_with_element_by_id('menu_main_bottom_nav_catalog', 0, 'click')
        self.test_screenshot()


    @allure.step('Переход в корзину')
    def test_go_to_bottom_cart(self):
        self.test_await_action_with_element_by_id(self.LOCATOR_BOTTOM_CART, 0, 'click')
        self.test_screenshot()


    @allure.step('Переход в профиль')
    def test_go_to_bottom_profile(self):
        self.test_await_action_with_element_by_id('menu_main_bottom_nav_profile', 0, 'click')
        self.test_screenshot()


    @allure.step('Переход в бонусы')
    def test_go_to_bottom_bonuses(self):
        self.test_await_action_with_element_by_id('menu_main_bottom_nav_bonuses', 0, 'click')
        self.test_screenshot()