import allure

from .base import TestBase


class TestProduct(TestBase):


    allure.step('Окрытие изображения товара')
    def test_product_image_open(self):
        self.test_await_action_with_element_by_id('product_image_pager', 0, 'click')
        self.test_screenshot()
    
    allure.step('Закрытие изображения товара')
    def test_product_image_close(self):
        self.test_await_action_with_element_by_id('image_pager_close_image', 0, 'click')
        self.test_screenshot()


    @allure.step('Добавление в избранное')
    def test_add_to_favorites(self):
        self.test_swipe_action_with_element_by_id('main_info_add_to_favorites_container', 0, 'click')
        self.test_screenshot()

    @allure.step('Добавление в сравнение')
    def test_add_to_comparison(self):
        self.test_swipe_action_with_element_by_id('main_info_add_to_comparison_container', 0, 'click')
        self.test_screenshot()


    @allure.step('Добавление в корзину')
    def test_add_to_cart(self):
        #self.test_swipe_action_with_element_by_id('offer_add_to_cart_button', 0, 'click') #############################################
        self.test_swipe()
        self.test_swipe()
        self.test_await_action_with_element_by_id('offer_add_to_cart_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Увеличение количества товара в корзине')
    def test_plus_to_cart(self):
        self.test_swipe_action_with_element_by_id('offer_plus_to_cart_label', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Переход в корзину')
    def test_go_to_cart(self):
        self.test_await_action_with_element_by_id('offer_go_to_cart_button', 0, 'click')
        self.test_screenshot()

    
    @allure.step('Быстрое оформление')
    def test_go_to_fast_order(self):
        #self.test_swipe_action_with_element_by_id('offer_fast_order_button', 0, 'click')
        self.test_swipe_to_list_element('offer_fast_order_button', 'Быстрое оформление').click()
        self.test_screenshot()

    @allure.step('Ввод номера телефона')
    def test_fast_order_phone_input(self, phone):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', phone)
        self.test_screenshot()

    @allure.step('Оформление заказа')
    def test_fast_order_action_button(self):
        self.test_await_action_with_element_by_id('fast_order_action_button', 0, 'click')
        self.test_screenshot()


    @allure.step('Нашли дешевле?')
    def test_find_cheaper(self):
        self.test_await_action_with_element_by_id('offer_found_cheaper_label', 0, 'click')
        self.test_screenshot()

    @allure.step('Ввод ссылки на товар')
    def test_find_cheaper_link_input(self, link):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', link)
        self.test_screenshot()

    @allure.step('Ввод номера телефона')
    def test_find_cheaper_phone_input(self, phone):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', phone)
        self.test_screenshot()

    @allure.step('Отправление заявки')
    def test_find_cheaper_action_button(self):
        self.test_await_action_with_element_by_id('found_cheaper_action_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Закрытие окна')
    def test_find_cheaper_close(self):
        self.test_await_action_with_element_by_id('dialog_toolbar_close_icon', 0, 'click')
        self.test_screenshot()

    

    @allure.step('Переход к Созданию отзыва')
    def test_review_add(self):
        self.test_swipe_action_with_element_by_id('review_question_add_label', 0, 'click')
        self.test_screenshot()

    @allure.step('Ввод имени')
    def test_review_name_input(self, name):
        self.test_swipe_action_with_element_by_id('main_text_input', 0, 'send_keys', name)
        self.test_screenshot()

    @allure.step('Ввод Email')
    def test_review_email_input(self, email):
        self.test_swipe_action_with_element_by_id('main_text_input', 1, 'send_keys', email)
        self.test_screenshot()

    @allure.step('Ввод достоинств')
    def test_review_advantages_input(self, advantages):
        self.test_swipe_action_with_element_by_id('main_text_input', 2, 'send_keys', advantages)
        self.test_screenshot()

    @allure.step('Ввод недостатков')
    def test_review_disadvantages_input(self, disadvantages):
        self.test_swipe_action_with_element_by_id('main_text_input', 3, 'send_keys', disadvantages)
        self.test_screenshot()

    @allure.step('Ввод комментария')
    def test_review_comment_input(self, comment):
        self.test_swipe_action_with_element_by_id('main_text_input', 4, 'send_keys', comment)
        self.test_screenshot()

    @allure.step('Создание отзыва')
    def test_review_action_button(self):
        self.test_swipe_action_with_element_by_id('add_review_action_button', 0, 'click')
        self.test_screenshot()

    

    @allure.step('Создание вопроса')
    def test_question_add(self):
        self.test_swipe_action_with_element_by_id('review_question_add_label', 1, 'click')
        self.test_screenshot()

    @allure.step('Ввод имени')
    def test_question_name_input(self, name):
        self.test_await_action_with_element_by_id('main_text_input', 0, 'send_keys', name)
        self.test_screenshot()

    @allure.step('Ввод Email')
    def test_question_email_input(self, email):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', email)
        self.test_screenshot()

    @allure.step('Ввод вопроса')
    def test_question_text_input(self, comment):
        self.test_await_action_with_element_by_id('main_text_input', 2, 'send_keys', comment)
        self.test_screenshot()

    @allure.step('Создание вопроса')
    def test_question_action_button(self):
        self.test_await_action_with_element_by_id('add_question_action_button', 0, 'click')
        self.test_screenshot()