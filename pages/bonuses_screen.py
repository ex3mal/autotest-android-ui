import allure
import pytest

from .base import TestBase


class TestBonuses(TestBase):

    @allure.step('Переход к оформлению бонусной карты')
    def test_go_to_design_card(self):
        self.test_await_action_with_element_by_id('bonus_card_add_button', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Ввод номера телефона')
    def test_design_card_phone_input(self, phone='9898875410'):
        self.test_await_action_with_element_by_id('sign_in_by_sms_phone_input', 0, 'send_keys', phone)
        self.test_screenshot()
    
    @allure.step('Переход на следующий этап')
    def test_design_card_phone_next_button(self):
        self.test_await_action_with_element_by_id('sign_in_by_sms_get_code_button', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Ввод кода подтверждения')
    def test_design_card_code_input(self, code='1111'):
        self.test_await_action_with_element_by_id('enter_code_input', 0, 'send_keys', code)
        self.test_screenshot()


    ########################### не требуется
    @allure.step('Ввод имени')
    def test_design_card_name_input(self, name):
        self.test_await_action_with_element_by_id('main_text_input', 1, 'send_keys', name)

    @allure.step('Ввод фамилии')
    def test_design_card_surname_input(self, surname):
        self.test_await_action_with_element_by_id('main_text_input', 2, 'send_keys', surname)

    
    @allure.step('Оформление бонусной карты')
    def test_design_card_action(self):
        self.test_await_action_with_element_by_id('add_bonus_card_confirm_action_button', 0, 'click')
        if self.test_check_page_with_element_by_id('snackbar_text'):
            pytest.skip('Ошибка при оформлении бонусной карты')


    @allure.step('Проверка оформления бонусной карты')
    def test_check_design_card_action(self):
        self.test_check_page_with_element_by_id('bonus_barcode_image')