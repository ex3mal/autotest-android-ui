import allure
from appium.webdriver.common.touch_action import TouchAction

from .base import TestBase


class DarkstoreMain(TestBase):
    @allure.step('Подтверждение условий работы')
    def test_working_conditions(self):
        self.test_await_action_with_element_by_id('ds_about_ok_button', 0, 'click')
        self.test_screenshot()

    @allure.step('Подтверждение доставки')
    def test_confirm_delivery(self):
        #self.test_timeout(10)
        self.test_await_action_with_element_by_id('ds_delivery_address_map_next_button', 0,'click')
        self.test_screenshot()

    @allure.step('Поиск')
    def test_search(self):
        self.test_await_action_with_element_by_id('ds_home_header_search_image', 0, 'click')
        self.test_screenshot()
    
    @allure.step('Переход в интернет магазин электроники')
    def test_go_to_05_ru(self):
        self.test_await_action_with_element_by_id('ds_item_home_banner_back_to_store_image', 0, 'click')
        self.test_screenshot()

    @allure.step('')
    def test_go_to_category(self):
        # self.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.FrameLayout/android.view.ViewGroup/androidx.cardview.widget.CardView[1]/android.widget.LinearLayout/android.widget.LinearLayout', \
        # 0, 'click', method='find_elements_by_xpath')
        self.test_timeout(5)
        TouchAction(self.driver).tap(None, 500, 1000, 1).perform()
        self.test_screenshot()