import allure
import time
import pytest_check
from selenium.common.exceptions import NoSuchElementException
from appium.webdriver.common.touch_action import TouchAction
from allure_commons.types import AttachmentType


class TestBase:

    interval = 1
    timeout = 20
    driver = None

    def __init__(self, driver):
        self.driver = driver
    
    def test_shake(self):
        self.timeout(2)
        self.driver.shake()


    def test_timeout(self, timeout=2):
        
        self.driver.implicitly_wait(timeout)

    def test_swipe(self):
        self.test_timeout(10)
        self.driver.swipe(500, 1000, 500, 500, 700)
        self.test_timeout(1)
    
    def test_swipe_for_bonus(self):
        self.driver.swipe(93, 1697, 280, 1697, 700)
        self.test_timeout(1)


    def test_swipe_horizont(self):
        self.test_timeout(2)
        self.driver.swipe(900, 1000, 100, 1000, 1000)
        self.test_timeout(1)
    
    def test_hide_keyboard(self):
        self.test_timeout(1)
        self.driver.hide_keyboard()

    def test_tap_enter_keyboard(self):
        self.driver.press_keycode(66)
    
        


    def test_click_center_screen(self):
        time.sleep(3)
        TouchAction(self.driver).tap(None, 500, 1000, 1).perform() # Привязка к координатам устройства    
    
    def test_click_by_coordinates(self, x, y):
        time.sleep(2)
        TouchAction(self.driver).tap(None, x, y, 1).perform()

    
    def test_swipe_to_list_element(self, element_id, element_text = ''):
        num = 0
        while True:
            try:
                foundElements = self.driver.find_elements_by_id(element_id)
                num += 1
            except:
                if num > 10:
                    break
                self.driver.swipe(500, 1500, 500, 500, 1000)
                continue
            if not element_text and len(foundElements):
                return foundElements
            for element in foundElements:
                if element.text == element_text:
                    return element
            #self.driver.swipe(500, 1500, 500, 500, 1000)
            self.test_swipe()


    def test_screenshot(self):
        time.sleep(1)
        self.test_timeout(1)
        #with allure.step('Делаем скриншот'):
        allure.attach(self.driver.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)


    def test_await_action_with_element_by_id(self, elementId, index, action, arguments = '', method = 'find_elements_by_id'):
        foundElement = self.test_await_element(elementId) # Ждать, пока не найдёт
        userAction = getattr(foundElement[index], action)
        if (len(arguments)):
            userAction(arguments)
        else:
            userAction()
        return foundElement


    def test_check_page_with_element_by_id(self, elementId, action = 'is_true'):
        foundElement = self.test_await_element(elementId) # Ждать, пока не найдёт
        getattr(pytest_check, action)(foundElement)    

    def test_check_text_page_with_element_by_id(self, elementId, elementIndex, text, action = 'is_true'):
        foundElement = self.test_await_element(elementId) # Ждать, пока не найдёт
        getattr(pytest_check, action)(foundElement[elementIndex].text == text)


    def test_await_element(self, elementId, method = 'find_elements_by_id'):
        end_time = time.time() + self.timeout
        while True:
            if time.time() > end_time:
                raise NoSuchElementException
            time.sleep(self.interval)
            foundElement = getattr(self.driver, method)(elementId)
            if not foundElement:
                continue
            return foundElement


    
    def test_swipe_element(self, elementId, action = 'find_elements_by_id'):
        end_time = time.time() + self.timeout
        while True:
            if time.time() > end_time:
                raise NoSuchElementException
            time.sleep(self.interval)
            foundElement = getattr(self.driver, action)(elementId)
            if not foundElement:
                #self.driver.swipe(500, 1500, 500, 500, 1000)
                self.test_swipe()
                continue
            return foundElement

    def test_swipe_action_with_element_by_id(self, elementId, index, action, arguments = '', method = 'find_elements_by_id', text = ''):
        foundElement = self.test_swipe_element(elementId) # Ждать, пока не найдёт
        userAction = getattr(foundElement[index], action)
        if (len(arguments)):
            userAction(arguments)
        else:
            userAction()
        return foundElement


    LOCATOR_BOTTOM_CART = 'menu_main_bottom_nav_cart'
    LOCATOR_CART_CLEAR = 'cart_product_clear_label'

    def test_clear_cart(self):
        self.test_await_action_with_element_by_id(self.LOCATOR_BOTTOM_CART, 0, 'click')
        self.test_screenshot()
        try:
            self.test_await_action_with_element_by_id(self.LOCATOR_CART_CLEAR, 0, 'click') 
        except:
            pass
            

    
        