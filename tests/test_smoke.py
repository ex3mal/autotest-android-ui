from select import select
import time
import allure
import pytest
import pytest_check
from appium.webdriver.common.touch_action import TouchAction

from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoSuchElementException

from pages.base import TestBase
from pages.bottom_bar import TestBottomBar
from pages.main_screen import TestMain
from pages.profile_screen import TestProfile, TestSignIn
from pages.authorized_profile_screen import TestAuthorizedProfile
from pages.catalog_screen import TestCatalog
from pages.cart_screen import TestCartIdle, TestCart, TestCartCheckout
from pages.product_screen import TestProduct
from pages.bonuses_screen import TestBonuses


@allure.feature('Профиль') 
@allure.severity('critical')
@pytest.mark.usefixtures('get_driver')
class TestSuiteProfile:

    #  Test1
    @allure.title("Авторизация по SMS коду")                  
    def test_case_sign_up(self):
        """Авторизация по SMS коду"""
        self.base = TestBase(self.driver) 
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

       
        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in() 
        self.signIn.test_sign_in_SMS_phone_input('9898875385')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()

        self.base.test_check_page_with_element_by_id('profile_menu_card_title_label')
        self.base.test_screenshot()


    # @allure.title("Повторная отправка кода подтверждения")
    # def test_case_repeat_send(self):
    #     """Повторная отправка кода подтверждения"""
    #     self.base = TestBase(self.driver) 
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in() 
    #     self.signIn.test_sign_in_SMS_phone_input('9898875410')  
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_repeat_send()
    #     self.signIn.test_sign_in_SMS_code_input()

    #     self.base.test_check_page_with_element_by_id('profile_menu_card_title_label')
    #     self.base.test_screenshot()

    # Test 2
    @allure.title("Выход из профиля")
    def test_case_exit(self):
        """Выход из профиля"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        #self.main.test_choice_other_city('Дербент')
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875385')
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.authorizedProfile.test_exit()          ##############
        
        self.base.test_check_page_with_element_by_id('profile_unauth_sign_in_button')
        self.base.test_screenshot()

    

#     # def test_case_recovery_password(self):
#     #     """Восстановаление пароля и авторизация по новому паролю"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     self.main.test_choice_city()
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_go_to_password_container()
#     #     self.signIn.test_go_to_recovery_password()
#     #     self.signIn.test_recovery_phone_email_input()
#     #     self.signIn.test_recovery_phone_email_next_button()
#     #     self.signIn.test_recovery_code_input()
#     #     self.signIn.test_recovery_code_next_button()
#     #     self.signIn.test_recovery_new_password_input('Qa123123?')
#     #     self.signIn.test_recovery_action_button()
#     #     time.sleep(2)
#     #     self.signIn.test_recovery_go_to_main()
#     #     time.sleep(2)
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_go_to_password_container()
#     #     self.signIn.test_sign_in_login_input()
#     #     self.signIn.test_sign_in_password_input('Qa123123?')
#     #     self.signIn.test_sign_in_action_button()

#     #     pytest_check.is_true()
#     #     self.base.test_screenshot()
    

#     # def test_case_change_password(self):
#     #     """Смена пароля и авторизация по новому паролю"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     self.main.test_choice_city()
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_SMS_phone_input()  
#     #     self.signIn.test_sign_in_SMS_get_code()
#     #     self.signIn.test_sign_in_SMS_code_input()
#     #     self.authorizedProfile.test_go_to_change_password()
#     #     self.authorizedProfile.test_change_password_old_input('Qa123123?')
#     #     self.authorizedProfile.test_change_password_new_input('Qa123123!')
#     #     self.authorizedProfile.test_change_password_action_button()
#     #     time.sleep(5)
#     #     self.authorizedProfile.test_exit()
#     #     time.sleep(5)
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_go_to_password_container()
#     #     self.signIn.test_sign_in_login_input()
#     #     self.signIn.test_sign_in_password_input('Qa123123!')
#     #     self.signIn.test_sign_in_action_button()
        
#     #     pytest_check.is_true()
#     #     self.base.test_screenshot()


#     # def test_case_change_personal_info(self):
#     #     """Изменение личной информации"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)
        
#     #     self.main.test_choice_city()
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_SMS_phone_input()  
#     #     self.signIn.test_sign_in_SMS_get_code()
#     #     self.signIn.test_sign_in_SMS_code_input()
#     #     self.authorizedProfile.test_go_to_personal_info()
#     #     self.authorizedProfile.test_personal_surname_input('Testov')
#     #     self.authorizedProfile.test_personal_patronymic_input('Testovich')
#     #     #self.authorizedProfile.test_personal_email_input('test05ru@list.ru')
#     #     self.authorizedProfile.test_personal_info_save()
#     #     time.sleep(5)
#     #     self.authorizedProfile.test_go_to_personal_info()
        
#     #     self.base.test_screenshot()


#     # @pytest.mark.parametrize('login, password',
#     #                         [
#     #                             ('89898875385', 'Qa123123!'),
#     #                             #('test05ru@list.ru', 'Qa123123!'),
#     #                         ]
#     #                         )
#     # def test_case_sign_in(self, login, password):
#     #     """Авторизация по паролю"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     self.main.test_choice_city()
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_go_to_password_container()
#     #     self.signIn.test_sign_in_login_input(login)
#     #     self.signIn.test_sign_in_password_input(password)
#     #     self.signIn.test_sign_in_action_button() 
        
#     #     pytest_check.is_true()
#     #     self.base.test_screenshot()

    
#     # def test_case_order_history_go_to_catalog(self):
#     #     """Переход из пустой истории заказов в каталог"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     self.main.test_choice_city()
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_SMS_phone_input()
#     #     self.signIn.test_sign_in_SMS_get_code()
#     #     self.signIn.test_sign_in_SMS_code_input()
#     #     self.authorizedProfile.test_go_to_order_history()
#     #     self.authorizedProfile.test_order_go_to_catalog()
        
#     #     pytest_check.is_true()
#     #     self.base.test_screenshot()

    # Test3
    @allure.title("Смена города")
    def test_case_change_city(self):
        """Смена города"""
        
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in() 
        self.signIn.test_sign_in_SMS_phone_input('9898875385')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.profile.test_go_to_change_city()
        self.profile.test_change_city('Дербент')

        self.base.test_check_text_page_with_element_by_id('home_search_label', 0, 'Быстрый поиск в г. Дербент')
        self.base.test_screenshot()

    
#     # @allure.title("Переход из пустого сравнения в каталог")
#     # def test_case_comparison_go_to_catalog(self):
#     #     """Переход из пустого сравнения в каталог"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     self.main.test_choice_city()
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_comparison()
#     #     self.profile.test_comparison_go_to_catalog()
        
#     #     self.base.test_check_text_page_with_element_by_id('main_toolbar_title_label', 0, 'Категории каталога')
#     #     self.base.test_screenshot()

    
#     # @allure.title("Переход из пустого избранного в каталог")
#     # def test_case_favoritest_go_to_catalog(self):
#     #     """Переход из пустого избранного в каталог"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     self.main.test_choice_city()
#     #     self.bottomBar.test_go_to_bottom_profile()
#     #     self.profile.test_go_to_sign_in()
#     #     self.signIn.test_sign_in_SMS_phone_input()
#     #     self.signIn.test_sign_in_SMS_get_code()
#     #     self.signIn.test_sign_in_SMS_code_input()
#     #     self.authorizedProfile.test_go_to_favorites()
#     #     self.authorizedProfile.test_favorites_go_to_catalog()
        
#     #     self.base.test_check_text_page_with_element_by_id('main_toolbar_title_label', 0, 'Категории каталога')
#     #     self.base.test_screenshot()



@allure.feature('Главная')
@allure.severity('critical')
@pytest.mark.usefixtures('get_driver')
class TestSuiteMain:

#     def test_case_go_to_categoty(self):
#         """Проверка перехода в категорию"""
#         self.base = TestBase(self.driver)
#         self.main = TestMain(self.driver)
#         self.bottomBar = TestBottomBar(self.driver)
#         self.catalog = TestCatalog(self.driver)
#         self.cartIdle = TestCartIdle(self.driver)
#         self.cart = TestCart(self.driver)
#         self.cartCheckout = TestCartCheckout(self.driver)
#         self.profile = TestProfile(self.driver)
#         self.signIn = TestSignIn(self.driver)
#         self.authorizedProfile = TestAuthorizedProfile(self.driver)
#         self.product = TestProduct(self.driver)
#         self.bonuses = TestBonuses(self.driver)
        
#         self.main.test_choice_city()
#         all_categories = ['Телефоны и гаджеты', 'Компьютерная техника', 'Офисная техника и сети', 'Телевизоры, аудио, видео',
#                         'Климатическая техника', 'Бытовая техника', 'Красота, здоровье', 'Спорт и одежда',
#                         'Бытовая химия и гигиена', 'Книги и канцтовары', 'Игры, развлечения, хобби',
#                         'Товары для дома', 'Автомобильная техника', 'Уцененные товары']
#         check = 0
#         all_results = ['Смартфоны', 'Комплектующие', 'Офисная техника', 'Телевизоры', 'Кондиционеры', 'Крупная бытовая техника',
#                         'Приборы для укладки', 'Фитнес и аксессуары', 'Средства для уборки', 'Школьные принадлежности',
#                         'Игровые приставки', 'Посуда', 'Электроника и техника', 'Бытовая техника']
#         for category in all_categories:
#             self.main.test_go_to_category(category)
            
#             #pytest_check.is_true()
#             check += 1
#             self.base.test_screenshot()
#             self.bottomBar.test_go_to_bottom_home()

    # Test 4,5
    @allure.title("Поиск")
    @pytest.mark.parametrize('search_query',
                            [
                                 ('iPhone'),
                                 ('Айфон'),
                            ]
                            )
    def test_case_search(self, search_query):
        """Поиск"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        time.sleep(4)
        TouchAction(self.driver).tap(None, 500, 130, 1).perform() # Привязка к координатам устройства 
        self.main.test_search_input(search_query)
        self.base.test_hide_keyboard()

        #self.base.test_check_page_with_element_by_id('')
        self.base.test_screenshot()

    @allure.title("Переход в результаты поиска")
    def test_case_go_to_results_search(self):
        """Переход в результаты поиска"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.base.test_await_action_with_element_by_id('home_search_container', 0, 'click')
        self.base.test_await_action_with_element_by_id('search_toolbar_query_input', 0, 'send_keys', 'айфон')
        self.base.test_tap_enter_keyboard()
        #self.base.test_click_by_coordinates(1000, 2200 )

        self.base.test_check_text_page_with_element_by_id('cart_counter_text_label', 0, 'В корзину')





    # Test 6
    @allure.title("Переход в Даркстор")
    def test_case_go_to_darkstore(self):
        """Переход в Даркстор"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        
        self.main.test_choice_city()
        self.main.test_go_to_darkstore()

        self.base.test_check_text_page_with_element_by_id('ds_about_ok_button', 0, 'Хорошо, буду знать')
        self.base.test_screenshot()
    

#     # @allure.title("Переход в баннер 'Телефоны и гаджеты'")
#     # def test_case_go_to_popular_product(self):
#     #     """Переход в баннер 'Телефоны и гаджеты'"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     self.main.test_choice_city()
#     #     self.main.test_go_to_phone_banner()
        
#     #     self.base.test_check_text_page_with_element_by_id('main_toolbar_title_label', 0, 'Телефоны и гаджеты')
#     #     self.base.test_screenshot()
    

#     # @allure.title("Открытие карточки товара в блоке 'Популярные товары'")
#     # def test_case_go_to_popular_product(self):
#     #     """Открытие карточки товара в блоке 'Популярные товары'"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)

#     #     #self.main.test_choice_city()
#     #     self.main.test_go_to_product_popular()
        
#     #     self.base.test_swipe_to_list_element('offer_fast_order_button', 'Быстрое оформление')
#     #     self.base.test_screenshot()


@allure.feature('Каталог')
@allure.severity('critical')
@pytest.mark.usefixtures('get_driver')
class TestSuiteCatalog:

    #Test7,8
    @allure.title("Поиск")
    @pytest.mark.parametrize('search_query',
                            [
                                 ('iPhone'),
                                 ('Айфон'),
                            ]
                            )
    def test_case_search(self, search_query):
        """Поиск"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        
        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        #time.sleep(3)
        #TouchAction(self.driver).tap(None, 1000, 130, 1).perform() # Привязка к координатам устройства 
        self.catalog.test_go_to_search()
        self.catalog.test_search_input(search_query)
        
        #pytest_check.is_true()
        self.base.test_screenshot()
    #Test 9
    @allure.title("Открытие карточки товара")
    def test_case_open_product_card(self):
        """Открытие карточки товара"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        
        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()    

        self.base.test_check_page_with_element_by_id('main_info_add_to_favorites_container')
        self.base.test_screenshot()


    #Test 10
    @allure.title("Добавление товара в корзину")    
    def test_case_add_product_card(self):
        """Добавление товара в корзину"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.bottomBar.test_go_to_bottom_cart()
        try:
            self.cart.test_product_clear()
        except:
            pass
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        try:
            self.catalog.test_add_to_cart()
        except:
            pass
        self.bottomBar.test_go_to_bottom_cart()
        
        self.base.test_check_text_page_with_element_by_id('cart_product_section_label', 0, 'В корзине 1 товар')
        self.base.test_screenshot()

    #Test 11
    @allure.title("Увеличение количества  товара в каталоге")    
    def test_case_counter_plus(self):
        """Увеличение количества  товара в каталоге"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.base.test_clear_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_add_to_cart()
        self.catalog.test_counter_plus()
       
        
        self.base.test_check_text_page_with_element_by_id('cart_counter_count_label', 0, '2')
        self.base.test_screenshot()    
        
    #Test 12
    @allure.title("Уменьшение количества  товара в каталоге")    
    def test_case_counter_minus(self):
        """Добавление товара в корзину"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.base.test_clear_cart()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_add_to_cart()
        self.catalog.test_counter_plus()
        self.catalog.test_counter_plus()
        self.catalog.test_counter_minus()
        
        self.base.test_check_text_page_with_element_by_id('cart_counter_count_label', 0, '2')
        self.base.test_screenshot()      
        
       
   

    
#     # @allure.title("Переход в баннер 'Телефоны и гаджеты'")
#     # def test_case_go_to_phone_banner(self):
#     #     """Переход в баннер 'Телефоны и гаджеты'"""
#     #     self.base = TestBase(self.driver)
#     #     self.main = TestMain(self.driver)
#     #     self.bottomBar = TestBottomBar(self.driver)
#     #     self.catalog = TestCatalog(self.driver)
#     #     self.cartIdle = TestCartIdle(self.driver)
#     #     self.cart = TestCart(self.driver)
#     #     self.cartCheckout = TestCartCheckout(self.driver)
#     #     self.profile = TestProfile(self.driver)
#     #     self.signIn = TestSignIn(self.driver)
#     #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
#     #     self.product = TestProduct(self.driver)
#     #     self.bonuses = TestBonuses(self.driver)
        
#     #     self.main.test_choice_city()
#     #     self.main.test_go_to_phone_banner()

#     #     self.base.test_check_text_page_with_element_by_id('main_toolbar_title_label', 0, 'Телефоны и гаджеты')
#     #     self.base.test_screenshot()



# # @allure.feature('Бонусы')
# # @allure.severity('critical')
# # @pytest.mark.usefixtures('get_driver')
# # class TestSuiteBonuses:

# #     @allure.title("Оформление бонусной карты")
# #     def test_case_design_card(self):
# #         """Оформление бонусной карты"""
# #         self.base = TestBase(self.driver)
# #         self.main = TestMain(self.driver)
# #         self.bottomBar = TestBottomBar(self.driver)
# #         self.catalog = TestCatalog(self.driver)
# #         self.cartIdle = TestCartIdle(self.driver)
# #         self.cart = TestCart(self.driver)
# #         self.cartCheckout = TestCartCheckout(self.driver)
# #         self.profile = TestProfile(self.driver)
# #         self.signIn = TestSignIn(self.driver)
# #         self.authorizedProfile = TestAuthorizedProfile(self.driver)
# #         self.product = TestProduct(self.driver)
# #         self.bonuses = TestBonuses(self.driver)
        
# #         self.main.test_choice_city()
# #         self.bottomBar.test_go_to_bottom_bonuses()
# #         self.bonuses.test_go_to_design_card()
# #         self.bonuses.test_design_card_phone_input()
# #         self.bonuses.test_design_card_phone_next_button()
# #         self.bonuses.test_design_card_code_input() #############
        
# #         self.base.test_check_text_page_with_element_by_id('bonus_card_show_qr_code_button', 0, 'Посмотреть QR код')
# #         self.base.test_screenshot()



@allure.feature('Оформление заказа')
@allure.severity('critical')
@pytest.mark.usefixtures('get_driver')
class TestSuiteCheckout:

    # #Test 13
    @allure.title("Оформление заказа авторизованным пользователем используя курьерскую доставку")
    def test_case_sign_in_checkout_1(self):
        """Оформление заказа авторизованным пользователем"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.base.test_clear_cart()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        try:
            self.product.test_add_to_cart()
        except:
            pass
        
           
        #self.catalog.test_go_to_product_card()
        #self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_go_to_checkout()
        #self.cartCheckout.test_delivery_method()
        self.cartCheckout.test_delivery_next_button() 
        #self.cartCheckout.test_address_choice()
        #self.base.test_click_center_screen()    # Привязка к координатам устройства 
        self.cartCheckout.test_delivery_address()
        self.cartCheckout.test_address_next_button()
        self.cartCheckout.test_contacts_next_button()
        #self.cartCheckout.test_payment_method()
        self.cartCheckout.test_payment_next_button()
        self.cartCheckout.test_checkout()
        
        self.base.test_check_page_with_element_by_id('item_order_created_header_image')
        self.base.test_screenshot()

    # # #Test 14
    @allure.title("Оформление заказа  авторизованным пользователем используя  Самовывоз")
    def test_case_sign_in_checkout_2(self):
        """Оформление заказа авторизованным пользователем"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        try:
            self.product.test_add_to_cart()
        except:
            pass
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_go_to_checkout()
        self.cartCheckout.test_delivery_method('Самовывоз')
        self.cartCheckout.test_delivery_next_button() 
        self.cartCheckout.test_delivery_pickup_choice()
        #self.cartCheckout.test_address_choice()
        #self.base.test_click_center_screen()    # Привязка к координатам устройства 
        
        self.cartCheckout.test_address_next_button()
        self.cartCheckout.test_contacts_next_button()
        #self.cartCheckout.test_payment_method()
        self.cartCheckout.test_payment_next_button()
        self.cartCheckout.test_checkout()
        
        self.base.test_check_page_with_element_by_id('item_order_created_header_image')
        self.base.test_screenshot()
           
       
    # #Test 15
    @allure.title("Оформление заказа авторизованным пользователем используя используя службу доставки DPD")
    def test_case_sign_in_checkout_3(self):
        """Оформление заказа авторизованным пользователем"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.profile.test_go_to_change_city()
        self.profile.test_change_city_with_search('Санкт-Петербург')
        
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        try:
            self.product.test_add_to_cart()
        except:
            pass
           
        #self.catalog.test_go_to_product_card()
        #self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_go_to_checkout()
        #self.cartCheckout.test_delivery_method()
        self.cartCheckout.test_delivery_next_button() 
        #self.cartCheckout.test_address_choice()
        #self.base.test_click_center_screen()    # Привязка к координатам устройства 
        self.cartCheckout.test_delivery_address()
        self.cartCheckout.test_address_next_button()
        self.cartCheckout.test_contacts_next_button()
        self.cartCheckout.test_payment_method()
        self.cartCheckout.test_payment_next_button()
        self.cartCheckout.test_checkout()
        
        self.base.test_check_page_with_element_by_id('item_order_created_header_image')
        self.base.test_screenshot()

    # #Test 16
    @allure.title("Оформление заказа авторизованным пользователем используя бонусы")
    def test_case_sign_in_checkout_4(self):
        """Оформление заказа авторизованным пользователем"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        try:
            self.product.test_add_to_cart()
        except:
            pass
           
        #self.catalog.test_go_to_product_card()
        #self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_go_to_checkout()
        #self.cartCheckout.test_delivery_method()
        self.cartCheckout.test_delivery_next_button() 
        self.driver.implicitly_wait(5)
        #self.cartCheckout.test_address_choice()
        #self.base.test_click_center_screen()    # Привязка к координатам устройства 
        self.cartCheckout.test_delivery_address()
        self.cartCheckout.test_address_next_button()
        self.cartCheckout.test_contacts_next_button()
        #self.cartCheckout.test_payment_method()
        self.cartCheckout.test_payment_next_button()
        self.driver.implicitly_wait(3)
        
        self.base.test_swipe()
        self.base.test_swipe_for_bonus()
        self.base.test_swipe()
        
        self.base.test_check_page_with_element_by_id('order_total_spent_bonuses_label')
        self.cartCheckout.test_checkout()

        
        self.base.test_check_page_with_element_by_id('item_order_created_header_image')
        self.base.test_screenshot()    




    @allure.title("Оформление заказа авторизованным пользователем используя промокод")
    def test_case_sign_in_checkout_5(self):
        """Оформление заказа авторизованным пользователем"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Бытовая химия и гигиена')
        self.catalog.test_go_to_category('Средства для уборки')
        self.catalog.test_go_to_category('Освежители и ароматизаторы')
        self.catalog.test_go_to_product_card()
        try:
            self.product.test_add_to_cart()
        except:
            pass
           
        #self.catalog.test_go_to_product_card()
        #self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_go_to_checkout()
        #self.cartCheckout.test_delivery_method()
        self.cartCheckout.test_delivery_next_button() 
        #self.cartCheckout.test_address_choice()
        #self.base.test_click_center_screen()    # Привязка к координатам устройства 
        self.cartCheckout.test_delivery_address()
        self.cartCheckout.test_address_next_button()
        self.cartCheckout.test_contacts_next_button()
        #self.cartCheckout.test_payment_method()
        self.cartCheckout.test_payment_next_button()
        self.base.test_swipe()
        self.base.test_swipe()
        self.base.test_swipe()
        self.cart.test_sign_in_promo_input()
        self.base.test_await_action_with_element_by_id('additional_send_promo_code_button', 0, 'click')
        self.base.test_check_page_with_element_by_id('order_total_discount_label')
        self.cartCheckout.test_checkout()
        

        self.base.test_check_page_with_element_by_id('item_order_created_header_image')
        self.base.test_screenshot()   


    @allure.title("Оформление заказа неавторизованным пользователем используя курьерскую доставку")
    def test_case_sign_in_checkout_6(self):
        """Оформление заказа неавторизованным пользователем"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
       
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        try:
            self.product.test_add_to_cart()
        except:
            pass
           
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_go_to_checkout()
        #self.cartCheckout.test_delivery_method()
        self.cartCheckout.test_delivery_next_button() 
        #self.cartCheckout.test_address_choice()
        #self.base.test_click_center_screen()    # Привязка к координатам устройства 
        self.cartCheckout.test_delivery_address()
        self.cartCheckout.test_address_next_button()
        self.cartCheckout.test_input_fio_in_contacts_step()
        self.cartCheckout.test_input_phone_number_in_contacts_step()
        self.cartCheckout.test_contacts_next_button()
        #self.cartCheckout.test_payment_method()
        self.cartCheckout.test_payment_next_button()
        self.cartCheckout.test_checkout()
        
        self.base.test_check_page_with_element_by_id('item_order_created_header_image')
        self.base.test_screenshot()

    @allure.title("Оформление заказа неавторизованным пользователем используя самовывоз")
    def test_case_sign_in_checkout_7(self):
        """Оформление заказа неавторизованным пользователем"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
       
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        try:
            self.product.test_add_to_cart()
        except:
            pass
           
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_go_to_checkout()
        self.cartCheckout.test_delivery_method('Самовывоз')
        self.cartCheckout.test_delivery_next_button() 
        self.cartCheckout.test_delivery_pickup_choice()
        self.cartCheckout.test_address_next_button()
        self.cartCheckout.test_input_fio_in_contacts_step()
        self.cartCheckout.test_input_phone_number_in_contacts_step()
        self.cartCheckout.test_contacts_next_button()
        #self.cartCheckout.test_payment_method()
        self.cartCheckout.test_payment_next_button()
        self.cartCheckout.test_checkout()
       
        
        self.base.test_check_page_with_element_by_id('item_order_created_header_image')
        self.base.test_screenshot()
        


    
        
        
       
         
        

        
        
    
    
    
    

    # @allure.title("Оформление заказа от 1000 руб авторизованным пользователем используя курьерскую доставку")
    # def test_case_sign_in_checkout_2(self):
    #     """Оформление заказа авторизованным пользователем"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
        
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)
        

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in()
    #     self.signIn.test_sign_in_SMS_phone_input()  
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_code_input()
        
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Книги и канцтовары')
    #     self.catalog.test_go_to_category('Книги')
    #     self.catalog.test_go_to_category('Бизнес и финансы')
    #     self.catalog.test_go_to_product_card_by_text('Книга "Разумный инвестор. Полное руководство по стоимостному инвестированию" | Бенджамин Грэм')
    #     self.product.test_add_to_cart()
    #     self.bottomBar.test_go_to_bottom_cart()
    #     self.cart.test_go_to_checkout()
    #     #self.cartCheckout.test_delivery_method()
    #     self.cartCheckout.test_delivery_next_button() 
    #     #self.cartCheckout.test_address_choice()
    #     #self.base.test_click_center_screen()    # Привязка к координатам устройства 
    #     self.cartCheckout.test_delivery_town('Ставрополь')
    #     self.cartCheckout.test_address_next_button()
    #     self.cartCheckout.test_contacts_next_button()
    #     #self.cartCheckout.test_payment_method()
    #     self.cartCheckout.test_payment_next_button()
    #     self.cartCheckout.test_checkout()
        
    #     self.base.test_check_page_with_element_by_id('success_icon_container')
    #     self.base.test_screenshot()
    
    
    # @allure.title("Оформление заказа авторизованным пользователем используя самовывоз")
    # def test_case_sign_in_checkout_3(self):
    #     """Оформление заказа авторизованным пользователем"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
        
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)
        

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in()
    #     self.signIn.test_sign_in_SMS_phone_input()  
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_code_input()
        
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Книги и канцтовары')
    #     self.catalog.test_go_to_category('Письменные принадлежности')
    #     self.catalog.test_go_to_category('Ручки')
    #     try:
    #         self.catalog.test_add_to_cart()
    #     except:
    #         pass
    #     #self.catalog.test_go_to_product_card()
    #     #self.product.test_add_to_cart()
    #     self.bottomBar.test_go_to_bottom_cart()
    #     self.cart.test_go_to_checkout()
    #     self.cartCheckout.test_delivery_method('Самовывоз')
    #     self.cartCheckout.test_delivery_next_button() 
    #     self.cartCheckout.test_delivery_pickup_choice()
    #     #self.cartCheckout.test_address_choice()
    #     #self.base.test_click_center_screen()    # Привязка к координатам устройства 
    #     self.cartCheckout.test_delivery_address()
    #     self.cartCheckout.test_address_next_button()
    #     self.cartCheckout.test_contacts_next_button()
    #     #self.cartCheckout.test_payment_method()
    #     self.cartCheckout.test_payment_next_button()
    #     self.cartCheckout.test_checkout()
        
    #     self.base.test_check_page_with_element_by_id('success_icon_container')
    #     self.base.test_screenshot()
    

    # @allure.title("Оформление заказа авторизованным пользователем используя службу доставки DPD")
    # def test_case_sign_in_checkout_4(self):
    #     """Оформление заказа авторизованным пользователем"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
        
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)
        

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in()
    #     self.signIn.test_sign_in_SMS_phone_input()  
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_code_input()
        
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Книги и канцтовары')
    #     self.catalog.test_go_to_category('Письменные принадлежности')
    #     self.catalog.test_go_to_category('Ручки')
    #     try:
    #         self.catalog.test_add_to_cart()
    #     except:
    #         pass
    #     #self.catalog.test_go_to_product_card()
    #     #self.product.test_add_to_cart()
    #     self.bottomBar.test_go_to_bottom_cart()
    #     self.cart.test_go_to_checkout()
    #     #self.cartCheckout.test_delivery_method()
    #     self.cartCheckout.test_delivery_next_button() 
    #     #self.cartCheckout.test_address_choice()
    #     #self.base.test_click_center_screen()    # Привязка к координатам устройства 
    #     self.cartCheckout.test_delivery_address()
    #     self.cartCheckout.test_address_next_button()
    #     self.cartCheckout.test_contacts_next_button()
    #     #self.cartCheckout.test_payment_method()
    #     self.cartCheckout.test_payment_next_button()
    #     self.cartCheckout.test_checkout()
        
    #     self.base.test_check_page_with_element_by_id('success_icon_container')
    #     self.base.test_screenshot()

    
    # @allure.title("Быстрое оформление авторизованным пользователем")
    # def test_case_sign_in_fast_order(self):
    #     """Быстрое оформление авторизованным пользователем"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)
            
    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in()
    #     self.signIn.test_sign_in_SMS_phone_input('9898875385')
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_code_input()
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Книги и канцтовары')
    #     self.catalog.test_go_to_category('Письменные принадлежности')
    #     self.catalog.test_go_to_category('Ручки')
    #     self.catalog.test_go_to_product_card()
    #     self.product.test_go_to_fast_order()
    #     self.product.test_fast_order_action_button()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.authorizedProfile.test_go_to_order_history()
        
    #     self.base.test_screenshot()


    # @allure.title("Отмена заказа")
    # def test_case_order_cancel(self):
    #     """Отмена заказа"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)

    #     self.main.test_choice_city()             
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in()
    #     self.signIn.test_sign_in_SMS_phone_input('9898875385')  
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_code_input()
        
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.authorizedProfile.test_go_to_order_history()
    #     self.authorizedProfile.test_go_to_last_order()
    #     self.authorizedProfile.test_order_cancel_next_button()
    #     self.authorizedProfile.test_order_cancel_reason()
    #     #self.authorizedProfile.test_order_cancel_problems_input()
    #     self.authorizedProfile.test_order_cancel_action()
       
    #     self.base.test_check_page_with_element_by_id('success_icon_container')
    #     self.base.test_screenshot()         


@allure.feature('Корзина')
@allure.severity('critical')
@pytest.mark.usefixtures('get_driver')
class TestSuiteCart:

    # Test10
    # @allure.title("Переход в каталог из пустой корзины")
    # def test_case_go_to_catalog(self):
    #     """Переход в каталог из пустой корзины"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)
            
    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_cart()
    #     self.cartIdle.test_go_to_catalog()
        
    #     self.base.test_check_text_page_with_element_by_id('main_toolbar_title_label', 0, 'Категории каталога')
    # self.base.test_screenshot()


    # Test11
    @allure.title("Добавление и удаление товара из корзины")    
    def test_case_delete_product(self):
        """Добавление и удаление товара из корзины"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()  
        self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_product_delete()
        
        self.base.test_check_text_page_with_element_by_id('empty_view_title_label', 0, 'В корзине нет товаров')
        self.base.test_screenshot()


    # Test12
    @allure.title("Добавление нескольких товаров в корзину")    
    def test_case_add_several_products(self):
        """Добавление нескольких товаров в корзину"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        try:
            self.catalog.test_add_to_cart()
            self.catalog.test_add_to_cart_2()
            self.bottomBar.test_go_to_bottom_cart()
        except:
            self.bottomBar.test_go_to_bottom_cart()
        
        self.base.test_check_text_page_with_element_by_id('cart_product_section_label', 0, 'В корзине 2 товара')
        self.base.test_screenshot()
    

    # Test13
    @allure.title("Очистка всей корзины")    
    def test_case_clear_product(self):
        """Очистка всей корзины"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        try:
            self.catalog.test_add_to_cart()
            self.catalog.test_add_to_cart_2()
            self.bottomBar.test_go_to_bottom_cart()
        except:
            self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_product_clear()
        
        self.base.test_check_text_page_with_element_by_id('empty_view_title_label', 0, 'В корзине нет товаров')
        self.base.test_screenshot()

    # Test14
    @allure.title("Увеличение количества товаров") 
    def test_case_counter_plus(self):
        """Увеличение количества товаров"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()  #######
        self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_counter_plus()
        
        
        self.base.test_check_text_page_with_element_by_id('cart_counter_value_label', 0, '2 шт')
        self.base.test_screenshot()

    # Test15
    @allure.title("Уменьшение количества товаров")
    def test_case_counter_minus(self):
        """Уменьшение количества товаров"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        self.cart.test_counter_plus()
        self.base.test_timeout(2)
        
        self.cart.test_counter_minus()
        
        self.base.test_check_text_page_with_element_by_id('cart_counter_value_label', 0, '1 шт')
        self.base.test_screenshot()

    # Test16
    # @allure.title("Добавление в сравнение из корзины")    
    # def test_case_add_to_comparison(self):
    #     """Добавление в сравнение из корзины"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Книги и канцтовары')
    #     self.catalog.test_go_to_category('Письменные принадлежности')
    #     self.catalog.test_go_to_category('Ручки')
    #     self.catalog.test_go_to_product_card()
    #     self.product.test_add_to_cart()
    #     self.bottomBar.test_go_to_bottom_cart()
    #     self.cart.test_add_to_comparison()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_comparison()
    #     #self.profile.test_comparison_choice_category()
        
    #     #self.base.test_check_text_page_with_element_by_id('', 0, '')
    #     self.base.test_screenshot()



@allure.feature('Карточка товара')
@allure.severity('critical')
@pytest.mark.usefixtures('get_driver')
class TestSuiteProductCard:

    @allure.title("Открытие  карточки товара")
    def test_case_open_product_card(self):
        """Открытие  карточки товара"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()

        self.base.test_check_page_with_element_by_id('product_toolbar_share_icon')        
        self.base.test_screenshot()


    @allure.title("Открытие  экрана рассрочки Ля-риба")
    def test_case_open_installment_screen (self):
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)
        

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Телефоны и гаджеты')
        self.catalog.test_go_to_category('Смартфоны')
        self.catalog.test_go_to_product_card()
        self.base.test_swipe_action_with_element_by_id('offer_buy_in_installments_button', 0, 'click')
       
        self.base.test_check_text_page_with_element_by_id('main_toolbar_title_label', 0, 'Рассрочка ЛяРиба')        
        self.base.test_screenshot()


    # # Test16
    @allure.title("Добавление в корзину")
    def test_case_add_to_cart(self):
        """Добавление в корзину"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        self.product.test_add_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
        
        self.base.test_check_text_page_with_element_by_id('cart_product_section_label', 0, 'В корзине 1 товар')
        self.base.test_screenshot()


    @allure.title("Увеличение количества товара")
    def test_case_add_to_product(self):
        """Открытие  карточки товара"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.base.test_clear_cart()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        self.product.test_add_to_cart()
        self.product.test_plus_to_cart()
        self.bottomBar.test_go_to_bottom_cart()
       
        
        

        self.base.test_check_text_page_with_element_by_id('cart_counter_value_label', 0, '2 шт')
        self.base.test_screenshot()


    @allure.title("Быстрое оформление заказа")
    def test_case_fast_order_making(self):
        """Быстрое оформление заказв"""
        self.base = TestBase(self.driver)
        self.main = TestMain(self.driver)
        self.bottomBar = TestBottomBar(self.driver)
        self.catalog = TestCatalog(self.driver)
        self.cartIdle = TestCartIdle(self.driver)
        self.cart = TestCart(self.driver)
        self.cartCheckout = TestCartCheckout(self.driver)
        self.profile = TestProfile(self.driver)
        self.signIn = TestSignIn(self.driver)
        self.authorizedProfile = TestAuthorizedProfile(self.driver)
        self.product = TestProduct(self.driver)
        self.bonuses = TestBonuses(self.driver)

        self.main.test_choice_city()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_sign_in()
        self.signIn.test_sign_in_SMS_phone_input('9898875410')  
        self.signIn.test_sign_in_SMS_get_code()
        self.signIn.test_sign_in_SMS_code_input()
        self.base.test_clear_cart()
        self.bottomBar.test_go_to_bottom_catalog()
        self.catalog.test_go_to_category('Книги и канцтовары')
        self.catalog.test_go_to_category('Письменные принадлежности')
        self.catalog.test_go_to_category('Ручки')
        self.catalog.test_go_to_product_card()
        self.product.test_go_to_fast_order()
        self.product.test_fast_order_phone_input('9999999999')
        self.product.test_fast_order_action_button()
        self.bottomBar.test_go_to_bottom_profile()
        self.profile.test_go_to_orders()
        self.base.test_click_by_coordinates(410, 460)
        
        

        self.base.test_check_text_page_with_element_by_id('order_total_prop_value_label', 5, '79999999999')

    # Test18
    # @allure.title("Добавление в корзину компьютеры 05")
    # def test_case_add_to_cart_comp_05ru(self):
    #     """Добавление в корзину компьютеры 05"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Компьютерная техника')
    #     self.catalog.test_go_to_category('Компьютеры 05.ru')
    #     self.catalog.test_go_to_product_card()
    #     self.product.test_add_to_cart()
    #     self.bottomBar.test_go_to_bottom_cart()
        
    #     self.base.test_check_text_page_with_element_by_id('cart_product_section_label', 0, 'В корзине 1 товар')
    #     self.base.test_screenshot()


    # @allure.title("Добавление в сравнение")
    # def test_case_add_to_comparison(self):
    #     """Добавление в сравнение"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Книги и канцтовары')
    #     self.catalog.test_go_to_category('Письменные принадлежности')
    #     self.catalog.test_go_to_category('Ручки')
    #     self.catalog.test_go_to_product_card()
    #     self.product.test_add_to_comparison()
        
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_comparison()
        
    #     self.base.test_await_action_with_element_by_id('comparison_category_name_label', 0, 'click')
        
    #     self.base.test_check_text_page_with_element_by_id('comparison_category_name_label', 0, 'Ручки 1')
    #     self.base.test_screenshot()


    # @allure.title("Добавление в сравнение компьютеры 05")
    # def test_case_add_to_comparison_comp_05ru(self):
    #     """Добавление в сравнение компьютеры 05"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)


    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Компьютерная техника')
    #     self.catalog.test_go_to_category('Компьютеры 05.ru')
    #     self.catalog.test_go_to_product_card()
    #     self.product.test_add_to_comparison()
        
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_comparison()
        
    #     self.base.test_await_action_with_element_by_id('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[1]/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.widget.TextView[2]', \
    #         0, 'click', method='find_elements_by_xpath')
        
    #     self.base.test_check_text_page_with_element_by_id('main_toolbar_title_label', 0, 'Компьютеры 05.ru 1')
    #     self.base.test_screenshot()

    
    # @allure.title("Добавление в избранное авторизованным пользователем")
    # def test_case_sign_in_add_to_favorites(self):
    #     """Добавление в избранное авторизованным пользователем"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in()
    #     self.signIn.test_sign_in_SMS_phone_input()
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_code_input()
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Книги и канцтовары')
    #     self.catalog.test_go_to_category('Письменные принадлежности')
    #     self.catalog.test_go_to_category('Ручки')
    #     self.catalog.test_go_to_product_card()
    #     self.product.test_add_to_favorites()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.authorizedProfile.test_go_to_favorites()
        
    #     self.base.test_check_page_with_element_by_id('cart_counter_default_container')
    #     self.base.test_screenshot()

    
    # @allure.title("Добавление в избранное компьютеры 05 авторизованным пользователем")
    # def test_case_sign_in_add_to_favorites_comp_05ru(self):
    #     """Добавление в избранное компьютеры 05 авторизованным пользователем"""
    #     self.base = TestBase(self.driver)
    #     self.main = TestMain(self.driver)
    #     self.bottomBar = TestBottomBar(self.driver)
    #     self.catalog = TestCatalog(self.driver)
    #     self.cartIdle = TestCartIdle(self.driver)
    #     self.cart = TestCart(self.driver)
    #     self.cartCheckout = TestCartCheckout(self.driver)
    #     self.profile = TestProfile(self.driver)
    #     self.signIn = TestSignIn(self.driver)
    #     self.authorizedProfile = TestAuthorizedProfile(self.driver)
    #     self.product = TestProduct(self.driver)
    #     self.bonuses = TestBonuses(self.driver)

    #     self.main.test_choice_city()
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.profile.test_go_to_sign_in()
    #     self.signIn.test_sign_in_SMS_phone_input()
    #     self.signIn.test_sign_in_SMS_get_code()
    #     self.signIn.test_sign_in_SMS_code_input()
    #     self.bottomBar.test_go_to_bottom_catalog()
    #     self.catalog.test_go_to_category('Компьютерная техника')
    #     self.catalog.test_go_to_category('Компьютеры 05.ru')
    #     self.catalog.test_go_to_product_card()
    #     self.product.test_add_to_favorites()
        
    #     self.bottomBar.test_go_to_bottom_profile()
    #     self.authorizedProfile.test_go_to_favorites()
        
    #     #pytest_check.is_true()
    #     self.base.test_screenshot()
