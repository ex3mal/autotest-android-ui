import allure
import pytest

from pages.base import TestBase
from pages.darkstore_screen import DarkstoreMain
from pages.main_screen import TestMain


@pytest.mark.usefixtures('get_driver')
class Test1:
    def test_open_category(self):
        
        self.main=TestMain(self.driver)
        self.dark=DarkstoreMain(self.driver)
        self.base=TestBase(self.driver)
        
        
        self.main.test_choice_city()
        self.main.test_go_to_darkstore()
        self.dark.test_working_conditions()
        self.dark.test_confirm_delivery()
        self.dark.test_go_to_category()
        self.base.test_screenshot()



    